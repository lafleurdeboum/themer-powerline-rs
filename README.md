# themer-powerline-rs

A [powerline-rs](https://gitlab.com/jD91mZM2/powerline-rs) template for
[themer](https://github.com/mjswensen/themer).

## Why

Why ? Because having consistent colors in different CLI tools can be an
unfinishable cursed nightmare. Base16 tried to address it, and themer took the
spirit a little further. With themer, you can have consistent colors across
`kitty`, `vim` (including `vim-fugitive`, `vim-gutter` and such),
`vim-lightline`, `powerline-rs`, `firefox`, `ls` ... Uh sorry, no, not `ls`.
That would be too easy. But you can write a themer for `ls`. If you do, please
tell me. Looking forward the great harmonization of all things !

## Installation & usage

Install this module wherever you have `themer` installed:

    npm install themer-powerline-rs

Then pass `themer-powerline-rs` as a `-t` (`--template`) arg to `themer`:

    themer -c my-colors.js -t themer-powerline-rs -o gen

Installation instructions for the generated theme(s) will be included in
`<output dir>/README.md`.
