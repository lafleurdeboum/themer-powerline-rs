// themer-powerline-rs
//
// A reminder of the codes used bu themer :
// ${colors.accent0}             // Red                  [Error]
// ${colors.accent1}             // Orange
// ${colors.accent2}             // Yellow               [Warning]
// ${colors.accent3}             // Green                [Success]
// ${colors.accent4}             // Cyan
// ${colors.accent5}             // Blue
// ${colors.accent6}             // Caret/cursor (?)

// ${colors.shade0}              // BG color
// ${colors.shade1}              // UI
// ${colors.shade2}              // UI text selection
// ${colors.shade3}              // UI code comments
// ${colors.shade4}              // UI (?)
// ${colors.shade5}              // UI (?)
// ${colors.shade6}              // Foreground text (?)
// ${colors.shade7}              // Foreground text (?)

const convert = require('color-convert');
// Copied from @themer/vim :
// HACK `color-convert`'s conversion to ANSI currently isn't that accurate for
// grays
const ansi256Colors = [];

for (let color = 0; color < 255; ++color) {
  const hexVal = convert.ansi256.hex(color);
  ansi256Colors.push(hexVal);
}

ansi256Colors.closest = function(hexVal) {
  const [r, g, b] = convert.hex.rgb(hexVal);
  let minDistance = Infinity;
  let index; // NOTE equals ansi code
  for (let i = 0; i < this.length; ++i) {
    const [otherR, otherG, otherB] = convert.hex.rgb(this[i]);
    const distance = (r - otherR) ** 2
      + (g - otherG) ** 2
      + (b - otherB) ** 2;
    if (distance < minDistance) {
      minDistance = distance;
      index = i;
    }
  }
  return index;
};

const gtermToCterm = hexVal => ansi256Colors.closest(hexVal);

const renderTheme = (colors, isDark) => `
ssh_char = 
git_staged_char = 

separator_fg = ${gtermToCterm(colors.shade3)}
home_bg = ${gtermToCterm(colors.accent5)}
home_fg = ${gtermToCterm(colors.shade0)}
path_bg = ${gtermToCterm(colors.shade2)}
path_fg = ${gtermToCterm(colors.shade6)}
cwd_fg = ${gtermToCterm(colors.shade7)}

username_bg = ${gtermToCterm(colors.shade3)}
username_fg = ${gtermToCterm(colors.shade6)}
username_root_bg = ${gtermToCterm(colors.accent0)}
hostname_bg = ${gtermToCterm(colors.shade2)}
hostname_fg = ${gtermToCterm(colors.shade6)}

jobs_bg = ${gtermToCterm(colors.shade1)}
jobs_fg = ${gtermToCterm(colors.accent5)}

time_bg = ${gtermToCterm(colors.shade1)}
time_fg = ${gtermToCterm(colors.shade6)}

ssh_bg = ${gtermToCterm(colors.accent3)}
ssh_fg = ${gtermToCterm(colors.shade1)}

ro_bg = ${gtermToCterm(colors.accent0)}
ro_fg = ${gtermToCterm(colors.shade7)}

git_clean_bg = ${gtermToCterm(colors.accent3)}
git_clean_fg = ${gtermToCterm(colors.shade6)}
git_dirty_bg = ${gtermToCterm(colors.accent4)}
git_dirty_fg = ${gtermToCterm(colors.shade6)}
git_ahead_bg = ${gtermToCterm(colors.shade3)}
git_ahead_fg = ${gtermToCterm(colors.shade6)}
git_behind_bg = ${gtermToCterm(colors.shade3)}
git_behind_fg = ${gtermToCterm(colors.shade6)}
git_conflicted_bg = ${gtermToCterm(colors.accent6)}
git_conflicted_fg = ${gtermToCterm(colors.shade6)}
git_notstaged_bg = ${gtermToCterm(colors.accent2)}
git_notstaged_fg = ${gtermToCterm(colors.shade6)}
git_staged_bg = ${gtermToCterm(colors.accent4)}
git_staged_fg = ${gtermToCterm(colors.shade6)}
git_untracked_bg = ${gtermToCterm(colors.accent1)}
git_untracked_fg = ${gtermToCterm(colors.shade6)}

cmd_passed_bg = ${gtermToCterm(colors.shade1)}
cmd_passed_fg = ${gtermToCterm(colors.shade7)}
cmd_failed_bg = ${gtermToCterm(colors.accent0)}
cmd_failed_fg = ${gtermToCterm(colors.shade1)}
`

const render = colors => Object.entries(colors).map(
  async ([name, colors]) => ({
    name: `themer-${name}.theme`,
    contents: Buffer.from(renderTheme(colors, name === 'dark'), 'utf8'),
  })
);

const renderInstructions = paths =>
  "Run `powerline-rs` as either :\n```\n"
  + paths.map(p =>
    'powerline-rs --theme ' + p).join("\n"
  ) + "\n```";

module.exports = {
  render,
  renderInstructions,
}
